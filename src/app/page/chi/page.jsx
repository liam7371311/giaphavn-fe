import { Image } from "antd";
import SiderBar from "../../components/SideBar";
import React from "react";

const ChiPage = () => (
  <div className="App">
    <div className="logo">
      <Image
        src="https://www.giaphavn.com/static/media/logo.6d2d1b6d.png"
        alt="logo"
      />
    </div>
    <div className="menu">
      <SiderBar />
    </div>
  </div>
);

export default ChiPage;
