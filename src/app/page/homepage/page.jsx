"use client";
import React from "react";
import { Layout, Menu, Space, theme } from "antd";
import SiderBar from "../../components/SideBar";
import RightSide from "../../components/RightBar";
// import { BrowserRouter as Router } from "react-router-dom";
const { Header, Content, Footer } = Layout;

const HomePage = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    // <Router>
      <Layout>
        <SiderBar />
        <Layout>
          <Header
            style={{
              padding: "0 16px",
              background: colorBgContainer,
              margin: "16px 16px 0",
              borderRadius: borderRadiusLG,
            }}
          >
            <div className="header-left">
              <h3>Hello, guy!</h3>
            </div>
          </Header>
          <Content
            style={{
              margin: "24px 16px 0",
            }}
          >
            <div
              style={{
                padding: 24,
                minHeight: 500,
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
              }}
            >
              content
            </div>
          </Content>
          <Footer
            style={{
              textAlign: "center",
            }}
          >
            LichBich Design ©{new Date().getFullYear()} Created by LichBich
          </Footer>
        </Layout>
        <RightSide />
      </Layout>
    // </Router>
  );
};
export default HomePage;
