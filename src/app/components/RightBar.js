"use client";
import React from "react";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";

const { Sider } = Layout;

const labels = ["Trang chủ", "Chi", "Họ", "Nhánh", "Thông tin"];
const items = [
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  UserOutlined,
].map((icon, index) => ({
  key: String(index + 1),
  icon: React.createElement(icon),
  label: labels[index],
}));
const RightSide = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Sider style={{ background: colorBgContainer, padding: 16 }} width={348}>
      <div style={{color:"black"}}>Profile, Tạo sự kiện</div>
    </Sider>
  );
};
export default RightSide;
