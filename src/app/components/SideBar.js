"use client";
import React from "react";
import { useState, useEffect } from "react";
import {
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from "@ant-design/icons";
import { Layout, Menu, theme } from "antd";
import Link from "next/link";
// import { useRouter } from "next/router";
// import { Link, useHistory, useNavigate } from "react-router-dom";
// import { BrowserRouter as Router } from "react-router-dom";


const {Sider } = Layout;

const items = [
  { icon: UserOutlined, label: "Trang chủ", path: "/page/homepage" },
  { icon: VideoCameraOutlined, label: "Chi", path: "/page/chi" },
  { icon: UploadOutlined, label: "Họ", path: "/page/ho" },
  { icon: UserOutlined, label: "Nhánh", path: "/page/nhanh" },
  { icon: UserOutlined, label: "Thông tin", path: "/page/information" },
].map((item, index) => ({
  key: String(index + 1),
  icon: React.createElement(item.icon),
  label: item.label,
  path: item.path,
}));

function useCurrentKey(items) {
  const [currentKey, setCurrentKey] = useState("");

  useEffect(() => {
    const handlePathChange = () => {
      if (typeof window !== "undefined") {
        const currentPath = window.location.pathname;
        const newCurrentKey = items.find(
          (item) => item.path === currentPath
        )?.key;
        setCurrentKey(newCurrentKey);
      }
    };

    window.addEventListener("popstate", handlePathChange);

    // Clean up the event listener when the component unmounts
    return () => {
      window.removeEventListener("popstate", handlePathChange);
    };
  }, [items]); // Add items to the dependency array

  return currentKey;
}

const SiderBar = () => {
// const history = useHistory();
// const navigate = useNavigate();
const currentKey = useCurrentKey(items);

console.log("currentKey", currentKey);

  const {
    token: { colorBgContainer, borderRadiusLG},
  } = theme.useToken();
  return (
    // <Router>
    <Sider
      //   style={{ height: "100%" }}
      style={{ background: colorBgContainer }}
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={(broken) => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}
    >
      <div className="demo-logo-vertical" />
      <Menu
        style={{
          border: "none",
          marginTop: "120px",
          gap: "20px",
          display: "flex",
          flexDirection: "column",
        }}
        // theme="dark"
        mode="inline"
        defaultSelectedKeys={[currentKey]}
      >
        {items.map((item) => (
          <Menu.Item key={item.key} icon={item.icon}>
            <Link href={item.path}>{item.label}</Link>
          </Menu.Item>
        ))}
      </Menu>
    </Sider>
    // </Router>
  );
};
export default SiderBar;
