import React from "react";
import { Button } from "antd";

const ButtonComponent = ({
  type,
  className,
  id,
  onClick,
  children,
  size,
  style,
}) => (
  <Button
    size={size}
    type={type ? type : "default"}
    className={className}
    id={id}
    onClick={onClick}
    style={{backgroundColor: "white", color: "black", borderRadius: "50px", fontFamily: "Be VietNam Pro, sans-serif", fontWeight: "600"}}
  >
    {children}
  </Button>
);

export default ButtonComponent;
