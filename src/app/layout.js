import { Inter } from "next/font/google";
import "./globals.css";

const roboto = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Gia phả Việt Nam",
};

export default function RootLayout({ children }) {
  return (
    <html lang="vn">
      <body style={{ fontFamily: "Be Viet Nam Pro" }}>
        {children}
      </body>
    </html>
  );
}
